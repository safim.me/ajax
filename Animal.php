<?php

class Animal{
   public $name;
   public $speak;
   public $has_tail;
   public $leg;
   function __construct($name, $speak, $has_tail, $leg)
   {
    $this->name = $name;
    $this->speak = $speak;
    $this->has_tail = $has_tail;
    $this->leg = $leg;

   }
}



// Don't touch the below lines
$dog = new Animal('dog', 'bark', true, 4); // name, speak, has_tail, leg

print($dog->name . ", "); // 'dog'
print($dog->speak . ", "); // 'bark'
print($dog->has_tail . ", "); // true
print($dog->leg); // '4'