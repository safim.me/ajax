<?php
class User {
  public $firstName;
  public function hello(){
    echo "hello, " .  $this -> firstName;
    return $this;
  }
public function register() {
    echo " >> registered";
    return $this;
}
public function mail() {
    echo " >> mail sent";
}
    
  
}

// Dont change the below code...
$user1 = new User();
$user1->firstName = "Jane";
$user1->hello()->register()->mail(); 